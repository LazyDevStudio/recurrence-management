<?php

use Timber\Post;
use Timber\PostCollection;

class RecurrenceFactory {

	public static function forCollection( PostCollection $events, bool $useParts ): self {
		return new self( $events->get_posts(), $useParts );
	}

	public static function forEvent( EventPost $event, bool $useParts ): self {
		return new self( [ $event ], $useParts );
	}

	private function __construct( private array $events, private bool $useParts  ) {}

	public function eventParts( Post $post, bool $recurs ): array {
		$parts = get_field( 'parts', $post->id );
		$items = [];
		if ( ! empty ( $parts ) ) {
			foreach ( array_filter( $parts, fn( $part ) => $part[ 'isotherevent' ] === false ) as $part ) {
				$strategy = $recurs
					? new RecurAsMixed( $post->id, $part, $post->occurrence_id )
					: new RecurAsPart( $post->id, $part, $post->occurrence_id );
				$items[]  = new Recurrence( $strategy, $post );
			}
		}

		return $items;
	}

	public function get(): ?RecurrenceCollection {
		$recurrences = [];
		foreach ( $this->events as $event ) {
			$hasParts = ! empty( get_field( 'has_parts', $event->id ) );
			if ( $hasParts && $this->useParts ) {
				array_push( $recurrences, ...$this->eventParts( $event, $event->recurs() ) );
			} else {
				$strategy = $event->recurs()
					? new RecurAsOcc( $event->id, $event->occurrence_id ?: 0 )
					: new RecurDontRecur( $event->id, $event->occurrence_id ?: 0 );
				$recurrences[] = new Recurrence( $strategy, $event );
			}
		}
		return RecurrenceCollection::fromArray( $recurrences );
	}


}

