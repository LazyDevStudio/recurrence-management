<?php

use Venues\VenuePost;
use Timber\Post;

class Recurrence {

	public function __construct( private RecurrenceData $data, public Post $post ) {}

	public function postID(): int {
		return $this->post->id;
	}

	public function occID(): int {
		return $this->data->occurrence_id;
	}

	public function recurs() {
		return $this->data->recurs();
	}

	public function isFirst(): bool {
		return $this->data->isFirst();
	}

	public function isNew(): bool {
		$postDate = new \DateTime( $this->post->date( 'Y-m-d' ) );
		$diff     = $postDate->diff( new \DateTime() );
		return $diff->days <= 3;
	}

	public function start(): \DateTimeImmutable {
		return $this->data->start();
	}

	public function end(): \DateTimeImmutable {
		return $this->data->end();
	}

	public function title(): string {
		return $this->data->title();
	}

	public function partTitle(): ?string {
		return $this->data->partTitle();
	}

	public function link(): string {
		return $this->post->link();
	}

	public function categories(): array {
		return $this->post->getCategories();
	}

	public function audience(): string {
		$audience = get_field( 'audience', $this->postID() );
		return $audience[ 'label' ];
	}

	public function levels(): ?string {
		return $this->post->getLevel();
	}

	// TODO: This should never be null, refactor EventPost
	public function fee(): ?Fee {
		return $this->data->fee() ?? $this->post->getFee();
	}

	public function staff(): array {
		return $this->data->staff() ?? $this->post->mainTeachers();
	}

	public function venue(): VenuePost {
		return $this->post->getVenue();
	}
}
