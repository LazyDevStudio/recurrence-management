<?php

class RecurAsPart implements RecurrenceData {

	public const EOFORMAT = DATETIMEOBJ;

	protected string $name = 'part';

	protected \DateTimeImmutable $start;
	protected \DateTimeImmutable $end;
	protected ?string $title;
	protected ?Fee $fee;

	/**
	 * Strategy for getting occurrence data for an event part.
	 * This should be refactored into occurrences one day...
	 *
	 * @param int $postID
	 * @param array $rowData
	 * @param int $occID
	 */
	public function __construct( protected int $postID, private array $rowData, private int $occID ) {
		$this->start = $this->setStart( $this->rowData );
		$this->end   = $this->setEnd( $this->rowData );
		$this->title = strlen( $rowData[ 'title' ] ) ? $rowData[ 'title' ] : null;
		$this->fee   = strlen( $rowData[ 'special_fee' ] ) ? Fee::fromString( 'part price', $rowData[ 'special_fee' ] ) : null;
	}

	protected function occID(): ?int {
		return $this->occID;
	}

	public function recurs(): bool {
		return true;
	}

	public function isFirst() {
		return false;
	}

	private function setStart( $rowData ): \DateTimeImmutable {
		$originalStart = \DateTimeImmutable::createFromMutable( eo_get_the_start( self::EOFORMAT, $this->postID, $this->occID ) );
		$date          = $rowData[ 'date' ];
		$startTime     = $rowData[ 'start_time' ];
		return ! empty( $date ) ? new \DateTimeImmutable( "$date $startTime" ) : $originalStart->setTime( ...$this->timeFromString( $startTime ) );
	}

	private function setEnd( $rowData ): \DateTimeImmutable {
		$originalEnd = \DateTimeImmutable::createFromMutable( eo_get_the_end( self::EOFORMAT, $this->postID, $this->occID ) );
		$date        = $rowData[ 'date' ];
		$endTime     = $rowData[ 'end_time' ];
		return ! empty( $date ) ? new \DateTimeImmutable( "$date $endTime" ) : $originalEnd->setTime( ...$this->timeFromString( $endTime ) );
	}

	public function start(): \DateTimeInterface {
		return $this->start;
	}

	public function end(): \DateTimeInterface {
		return $this->end;
	}

	public function title(): string {
		$evtTitle = get_the_title( $this->postID );
		return $this->title ? "$evtTitle: $this->title" : $evtTitle;
	}

	public function partTitle(): ?string {
		return $this->title;
	}

	public function fee(): ?Fee {
		return $this->fee ?? null;
	}

	public function staff(): ?array {
		return null;
	}

	public function levels(): ?string {
		// TODO: As long as there is not field, use parent method.
		return null;
	}

	private function timeFromString( string $time ): array {
		return explode( ':', $time, 2 );
	}
}
