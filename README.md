This is the real recurrence management running at noutango.berlin. For legal reasons neither the entire plug-in nor complete namespaces are included here but only recurrence generation and querying as well as a "TeacherPost" entity, which shows exemplary use of the repository. This project exists only for demonstration purposes and must not be replicated or used elsewhere. © 2024. All rights reserved.


**Use for a page listing all workshops** (show recurrences, do not show parts):

```php
$builder = new EventQuery();
$getHandler = new GetRecurrences();
$events = $builder->after( new DateTimeImmutable('today 8am') )->inCategory( ['workshop'] );
$recurrences = $getHandler->run( $events, false );

$context['recurrences'] = $recurrences->all();
$context['events'] = $events->getUnique();
```


**Use for a programme calendar** (show recurrences, show parts):

```php
$start = new DateTimeImmutable( 'today' );
$end   = $start->modify( '+2 months' );
$builder     = new EventQueryBuilder();
$getHandler  = new GetRecurrences();
$events      = $builder->after( $start )->before( $end );
$recurrences = $getHandler->run( $events );

$context[ 'days' ]   = $recurrences->byDay();
$context[ 'events' ] = $events->getUnique();
```


**Use in single event view** (show recurrences, show parts):

```php
$query       = new EventQueryBuilder();
$getHandler  = new GetRecurrencesByEvent();
$recurrences = $getHandler->run( $this );
```

And then in your **twig template** somewhere...

```twig
{% for recurrence in event.recurrences.upcoming %}
    // Show your event recurrences :-)
{% endfor %}
```



