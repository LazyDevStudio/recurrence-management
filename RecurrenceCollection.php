<?php

namespace Events;

use Teachers\TeacherPost;

class RecurrenceCollection {

	public static function fromArray( array $array ) {
		$clean = array_filter( $array, fn( $item ) => $item instanceof Recurrence );
		return new self( $clean );
	}

	private function __construct( private array $recurrences ) {
		usort( $this->recurrences, fn( $a, $b ) => $a->start() <=> $b->start() );
	}

	public function all(): array {
		return $this->recurrences;
	}

	public function upcoming(): array {
		$now = new \DateTime( 'now' );
		return array_filter( $this->recurrences, fn( $item ) => $item->start() >= $now );
	}

	public function allReversed(): array {
		return array_reverse( $this->byDate() );
	}

	public function byDay( ?\DateTimeImmutable $start = null, ?\DateTimeImmutable $end = null ): array {
		$days = [];
		if ( current( $this->recurrences ) ) {
			usort( $this->recurrences, fn( $a, $b ) => $a->start() <=> $b->start() );
			$start    = $start ?? $this->recurrences[ 0 ]->start();
			$end      = $end ?? $this->recurrences[ array_key_last( $this->recurrences ) ]->start();
			$interval = new \DateInterval( "P1D" );
			$period   = new \DatePeriod( $start->setTime( 00, 00 ), $interval, $end->setTime( 23, 59 ), \DatePeriod::INCLUDE_END_DATE );
			foreach ( $period as $day ) {
				$days[ $day->format( 'Y-m-d' ) ] = array_filter( $this->recurrences, fn( $recurrence ) => $day->format( 'Y-m-d' ) === $recurrence->start()->format( 'Y-m-d' ) );
			}
		}

		return $days;
	}

	public function forStaff( TeacherPost $person ) {
		foreach ( $this->recurrences as $k => $data ) {
			$appearances = array_filter( $data->staff(), fn( $member ) => $member->name() === $person->name() );
			if ( ! current( $appearances ) ) {
				unset( $this->recurrences[ $k ] );
			}
		}
		return $this->recurrences;
	}

	public function onlyParts(): array {
		return array_filter( $this->recurrences, fn( $item ) => $item->isPart() );
	}

	public function next() {
		return current( $this->upcoming() ) ? $this->upcoming()[ 0 ] : null;
	}

	public function find( \DateTimeInterface $date ): ?Recurrence {
		$occs = array_filter( $this->upcoming(), fn( $occ ) => $occ->start() === $date );

		return current( $occs ) ? $occs[ array_key_first( $occs ) ] : null;
	}


}
