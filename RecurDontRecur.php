<?php


class RecurDontRecur implements RecurrenceData {

	public const EOFORMAT = DATETIMEOBJ;

	private string $name = 'one-off';


/**
* Strategy for getting occurrence data for a event that does not recur
*
* @param int $postID
* @param int $occID
*/
	public function __construct( private int $postID, private int $occID ) {}

	protected function occID(): ?int {
		return $this->occID;
	}

	public function recurs(): bool {
		return false;
	}

	public function isFirst(): bool {
		return false;
	}

	public function start(): \DateTimeImmutable {
		return \DateTimeImmutable::createFromMutable( eo_get_the_start( self::EOFORMAT, $this->postID, $this->occID ) );
	}

	public function end(): \DateTimeImmutable {
		return \DateTimeImmutable::createFromMutable( eo_get_the_end( self::EOFORMAT, $this->postID, $this->occID ) );
	}

	public function title(): string {
		return get_the_title( $this->postID );
	}

	public function levels(): ?string {
		return null;
	}

	public function fee(): ?Fee {
		return null;
	}

	public function staff(): ?array {
		return null;
	}

}
