<?php

namespace Nou\Events;

use Nou\Events\Fee;

class FeeFlexi extends Fee {

	private float $min = 0;
	private float $max = 0;


	protected function __construct( private string $name, float|string $min, float|string $max, string $currency = '&euro;' ) {
		parent::__construct( $name, $min, $currency );

		$this->isMulti = true;
		$this->min = floatval( $min );
		$this->max = floatval( $max );
	}

	public function min(): string {
		return "{$this->min}&thinsp;{$this->currency}";
	}

	public function max(): string {
		return "{$this->max}&thinsp;{$this->currency}";
	}

	public function minValue(): float {
		return $this->min;
	}

	public function maxValue(): float {
		return $this->max;
	}

	public function price(): string {
		return "{$this->min}&thinsp;&dash;&thinsp;{$this->max}&thinsp;{$this->currency}";
	}

	public function isFree(): bool {
		return false;
	}


}