<?php

interface RecurrenceData {

	public function recurs(): bool;

	public function start(): \DateTimeInterface;

	public function end(): \DateTimeInterface;

	public function title(): string;

	public function levels(): ?string;

	public function fee(): ?Fee;

	public function staff(): ?array;
}
