<?php

namespace Events;

use Teachers\TeacherPost;
use Timber\PostCollection;

class GetRecurrences {

	public function __construct() {}

	public function run( EventQueryBuilder $query, bool $useParts = true ): RecurrenceCollection {
		$recurrences = CreateRecurrences::forCollection( $query->get(), $useParts );
		return $recurrences->get();
	}

}
