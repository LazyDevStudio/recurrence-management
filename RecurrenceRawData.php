<?php

use http\Exception\InvalidArgumentException;
use Events\Fee;
use Teachers\Teacher;

class RecurrenceRawData {

	/**
	 * @param array $row
	 *
	 * @return RecurrenceRawData
	 * @throws \Exception
	 */
	public static function fromOcc( array $row ): RecurrenceRawData {
		if ( ! array_key_exists( 'date', $row ) || $row[ 'date' ] === '' ) {
			throw new \InvalidArgumentException( "The recurrence data array must contain a date." );
		}

		// this is a disaster because of ACFs unreliable return values
		$date   = new \DateTimeImmutable( $row[ 'date' ] );
		$fee    = $row[ 'special_fee' ] && $row[ 'special_fee' ] !== '' ? $row[ 'special_fee' ] : null;
		$team   = $row[ 'sub_team' ] ? $row[ 'sub_team' ] : [];
		$guests = $row[ 'sub_guest' ] && $row[ 'sub_guest' ] !== '' ? $row[ 'sub_guest' ] : null;
		$title  = $row[ 'title' ] && $row[ 'title' ] !== '' ? $row[ 'title' ] : null;

		return new self( $date, $team, $guests, $fee, $title );
	}

	/**
	 * Normalises occurrence fields data
	 *
	 * @param \DateTimeImmutable $date
	 * @param array $team
	 * @param array $guests
	 * @param string|null $fee
	 * @param string|null $title
	 */
	public function __construct(
		private \DateTimeImmutable $date,
		private array $team,
		private ?string $guests,
		private ?string $fee,
		private ?string $title
	) {}

	public function fee(): ?Fee {
		return $this->fee ? Fee::fromString( 'Special Fee', $this->fee ) : null;
	}

	public function team(): array {
		return array_map( fn( $member ) => Teacher::fromPost( $member ), $this->team );
	}

	public function guests(): array {
		return $this->guests
			? array_map( fn( $member ) => Teacher::fromString( trim( $member ) ), explode( ',', $this->guests ) )
			: [];
	}

	public function title(): ?string {
		return $this->title ?? null;
	}

}
