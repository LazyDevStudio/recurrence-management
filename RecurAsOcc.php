<?php

use Helpers\ACFHelper;


class RecurAsOcc implements RecurrenceData {

	public const EOFORMAT = DATETIMEOBJ;

	private string $name = 'occurrence';

	private ?RecurrenceRawData $rawData;

	/**
	 * Strategy for getting occurrence data of a recurring event
	 *
	 * @param int $postID
	 * @param int $occID
	 */
	public function __construct( private int $postID, private int $occID ) {
		$data = ACFHelper::get( 'substitutions', $this->postID );

		if ( $data ) {
			$occ           = array_filter( $data, fn( $row ) => $row[ 'date' ] === $this->start()->format( 'Y-m-d' ) );
			$key           = array_key_first( $occ );
			$this->rawData = ! is_null($key) ? RecurrenceRawData::fromOcc( $occ[ $key ] ) : null; 
		}
	}

	private function data(): ?RecurrenceRawData {
		return $this->rawData ?? null;
	}

	protected function occID(): ?int {
		return $this->occID;
	}

	public function recurs(): bool {
		return true;
	}

	public function isFirst(): bool {
		$schedule = eo_get_event_schedule( $this->postID );
		return $schedule[ 'start' ] === $this->start();
	}

	public function start(): \DateTimeImmutable {
		$occ  = eo_get_the_occurrence( $this->postID, $this->occID );
		$orig = eo_get_the_start( self::EOFORMAT, $this->postID );
		return \DateTimeImmutable::createFromMutable( $occ ? $occ[ 'start' ] : $orig );
	}

	public function end(): \DateTimeImmutable {
		$occ  = eo_get_the_occurrence( $this->postID, $this->occID );
		$orig = eo_get_the_end( self::EOFORMAT, $this->postID );
		return \DateTimeImmutable::createFromMutable( $occ ? $occ[ 'end' ] : $orig );
	}

	public function title(): string {
		return $this->data()?->title() ? get_the_title( $this->postID ) .": ". $this->data()->title() : get_the_title( $this->postID );
	}

	public function partTitle(): ?string {
		return $this->data()?->title();
	}

	public function levels(): ?string {
		return null;
	}

	public function fee(): ?Fee {
		return $this->data()?->fee() ?? null;
	}

	public function staff(): ?array {
		return $this->data()?->team() || $this->data()?->guests() ? [ ...$this->data()->team(), ...$this->data()->guests() ] : null;
	}
}
