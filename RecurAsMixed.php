<?php


class RecurAsMixed extends RecurAsPart implements RecurrenceData {

	protected string $name = 'occurrence with parts';

	private ?RecurrenceRawData $rawData;

	/**
	 * Strategy for getting occurrence data for an a recurring event with parts
	 *
	 * @param int $postID
	 * @param array $rowData
	 * @param int $occID
	 */
	public function __construct( int $postID, array $rowData, int $occID ) {
		parent::__construct( $postID, $rowData, $occID );
		$data = ACFHelper::get( 'substitutions', $this->postID );

		if ( $data ) {
			$occ = array_filter( $data, fn( $row ) => $row[ 'date' ] === $this->start()->format( 'Y-m-d' ) );
			$key = array_key_first($occ);
			$this->rawData = $key ? RecurrenceRawData::fromOcc( $occ[$key] ) : null;
		}
	}

	private function data(): ?RecurrenceRawData {
		return $this->rawData ?? null;
	}

	public function isFirst() {
		$schedule = eo_get_event_schedule( $this->postID );
		return $schedule[ 'start' ] === $this->start();
	}

	public function fee(): ?Fee {
		return $this->data()?->fee() ?? null;
	}

	public function staff(): ?array {
		return $this->data() ? [ ...$this->data()->team(), ...$this->data()->guests() ] : null;
	}


}
