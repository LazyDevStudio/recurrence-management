<?php

use Events\EventQueryBuilder;
use Events\GetRecurrences;
use Videos\VideoPost;
use Timber\Post;
use Timber\Timber;


class TeacherPost extends Post {

	private EventQueryBuilder $query;
	private GetRecurrences $getHandler;

	public function __construct( $pid = null ) {
		parent::__construct( $pid );
		$this->query = new EventQueryBuilder();
		$this->getHandler  = new GetRecurrences();
	}

	public function name(): string {
		return $this->title();
	}

	public function firstname(): string {
		return strtok( $this->title(), " " );
	}

	public function email(): ?string {
		return $this->meta( 'email' );
	}

	public function phone(): ?string {
		return $this->meta( 'phone' );
	}

	public function country(): ?string {
		return $this->meta( 'country' );
	}

	public function role(): ?string {
		return $this->meta( 'role' );
	}

	public function desc(): ?string {
		return $this->meta( 'alt_desc' ) ?? null;
	}

	public function video(): VideoPost {
		return Timber::get_post( $this->meta( 'video' ), "VideoPost" );
	}

	public function nextWorkshops(): array {
		$start  = new \DateTimeImmutable('now');
		$end    = $start->modify( '+3 months' );
		$events = $this->query->after( $start )->before( $end )->inCategory( ['workshop'] );
		$recurrences = $this->getHandler->run( $events, false );
		$byStaff = $recurrences->forStaff( $this );
		$this->query->reset();

		return $byStaff;
	}

	public function nextTrips() {
		$start  = new \DateTimeImmutable('now');
		$end    = $start->modify( '+6 months' );
		$events = $this->query->after( $start )->before( $end )->inCategory( ['reise'] );
		$recurrences = $this->getHandler->run( $events, false );
		$byStaff = $recurrences->forStaff( $this );
		$this->query->reset();

		return $byStaff;
	}


	public function nextClasses() {
		$start   = new \DateTimeImmutable( 'now' );
		$end     = $start->modify( '+1 week' );
		$events = $this->query->after( $start )->before( $end )->inCategory( ['kurs'] );
		$recurrences = $this->getHandler->run( $events, false );
		$byStaff = $recurrences->forStaff( $this );
		$this->query->reset();

		return $byStaff;
	}

	public function nextDJ() {
		$start   = new \DateTimeImmutable( 'now' );
		$end     = $start->modify( '+4 weeks' );
		$events = $this->query->after( $start )->before( $end )->inCategory( ['milonga'] );
		$recurrences = $this->getHandler->run( $events, false );
		$byStaff = $recurrences->forStaff( $this );
		$this->query->reset();

		return $byStaff;
	}

}
