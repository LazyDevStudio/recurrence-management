<?php

class Fee {

	protected bool $isMulti = false;

	private float $price;

	public static function fromString( string $name, string $value, string $currency = '&euro;' ): Fee {
		$parts = explode( '-', $value );
		if ( $parts && count( $parts ) > 1 ) {
			$min = trim( $parts[ 0 ] );
			$max = trim( str_replace( [ 'euro', '&euro;' ], "", $parts[ 1 ] ) );
			return new FeeFlexi( $name, $min, $max );
		}

		return new Fee( $name, $parts[ 0 ] );
	}

	public static function fromArray( string $name, array $values, string $currency = '&euro;' ): FeeFlexi {
		return new FeeFlexi( $name, $values[ array_key_first( $values ) ], $values[ array_key_last( $values ) ], $currency = '&euro;' );
	}

	protected function __construct( private string $name, float|string $price, public string $currency = '&euro;' ) {
		$this->price = floatval( $price );
	}

	public function price(): string {
		return "{$this->price}&thinsp;{$this->currency}";
	}

	public function priceValue(): float {
		return $this->price;
	}

	public function isFree(): bool {
		return !$this->priceValue() > 0;
	}

	public function name(): string {
		return $this->name;
	}

	public function isMulti(): bool {
		return $this->isMulti;
	}

	public function currency(): string {
		return $this->currency;
	}

}
