<?php

namespace Events;

class GetRecurrencesByEvent {

	public function __construct() {}

	public function run( EventPost $event, bool $useParts = true ): RecurrenceCollection {
		$recurrences = CreateRecurrences::forEvent( $event, $useParts );
		return $recurrences->get();
	}
}
